require 'rails_helper'

RSpec.describe UsersController, :type => :controller do
  describe 'app_exists?' do
    it 'finds app in db and returns true' do
      App.create(app_id: 'abc123', app_name: 'App Chi')

      get :app_exists?, :app_id => 'abc123'
      assigns(:app_exist).should be true
    end

    it 'fails to find app in db and returns false' do
      App.create(app_id: 'abc123', app_name: 'App Chi')

      get :app_exists?, :app_id => 'abc12312412'
      assigns(:app_exist).should be false
    end
  end

  describe 'user_exists?' do
    it 'finds user in DB and returns true' do
      User.create(username: 'Mer0J|4d0H', email: 'monster69@mail.ru', password: 'iloveyou')

      get :user_exists?, :email => 'monster69@mail.ru'
      assigns(:user_exist).should be true
    end

    it 'fails to find user in DB and returns false' do
      User.create(username: 'Mer0J|4d0H', email: 'monster69@mail.ru', password: 'iloveyou')

      get :user_exists?, :email => 'monster69@mail.com'
      assigns(:user_exist).should be false
    end

  end

  describe 'get_token' do
    it 'generates a token' do
      get :get_token, app_id: 'abc123', email: 'monster69@mail.ru'
      assigns(:gen_token).should_not be nil
    end
  end

  describe 'valid_pass?' do
    it 'confirms that provided password belongs to user' do
      User.create(username: 'Mer0J|4d0H', email: 'monster69@mail.ru', password: 'iloveyou')

      get :valid_pass?, email: 'monster69@mail.ru', password: 'iloveyou'
      assigns(:valid_pass).should be true
    end

    it 'fails to confirm that provided password belongs to user' do
      User.create(username: 'Mer0J|4d0H', email: 'monster69@mail.ru', password: 'iloveyou')

      get :valid_pass?, email: 'monster69@mail.ru', password: 'iloveyouasa'
      assigns(:valid_pass).should be false
    end
  end

  describe 'valid_token?' do
    it 'checks whether provided token is assigned to a user & app and returns true' do
      Log.create(email: 'monster69@mail.ru', app_id: 'abc123', token: '1234567890', time: DateTime.now)
      
      get :valid_token?, email: 'monster69@mail.ru', app_id: 'abc123', token: '1234567890'
      assigns(:valid_token).should be true
    end

    it 'checks whether provided token is assigned to a user & app and returns false' do
      Log.create(email: 'monster69@mail.ru', app_id: 'abc123', token: '1234567890', time: DateTime.now)
      
      get :valid_token?, email: 'monst1er69@mail.ru', app_id: '1abc123', token: 'asd1234567890'
      assigns(:valid_token).should be false
    end
  end
end