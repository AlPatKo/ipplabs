class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :app_id
      t.string :email
      t.string :token
      t.datetime :time

      t.timestamps null: false
    end
  end
end
