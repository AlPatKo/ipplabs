require 'digest'

class UsersController < ApplicationController
	wrap_parameters format: [:json]
	def app_exists?
		@app_exist = App.exists?(app_id: params[:app_id])
		message = "App(#{params[:app_id]}) " + ( @app_exist ? 'exists' : 'does not exist')
		render :text => message
	end

	def user_exists?
		@user_exist = User.exists?(email: params[:email])
		render :text => @user_exist.to_s
	end

	def get_token
		seed = params[:app_id].to_s + params[:email].to_s + DateTime.now.to_s
		@gen_token = token = Digest::MD5.hexdigest seed
		render :text => @gen_token.to_s
	end

	def valid_pass?
		@valid_pass = User.exists?(email: params[:email], password: params[:password])
		render :text => @valid_pass.to_s
	end

	def login
		if app_exists? && valid_pass?
			@code = 0
			@token = get_token
			create_log(params[:app_id], params[:email], @token)
		else
			@code = 2
			@token = nil
		end
		render_response(@code, @token)				
	end

	def valid_token?
		@valid_token = Log.exists?(email: params[:email], token: params[:token], app_id: params[:app_id])
		render :text => @valid_token.to_s
	end

	def get_last_login
		if !app_exists?
			@code = 2
		elsif !valid_token?
			@code = 3
		else
			log = Log.find_by(email: params[:email], token: params[:token], app_id: params[:app_id])
			@code = 0
			@time = log.time
			
		end
		render :json => {code: @code, time: @time}
				
	end

	def render_response(code, token)
		render :json => {code: code, token: token}
	end

	def create_log(app_id, email, token)
		l = Log.new(app_id: app_id, email: email, token: token, time: DateTime.now)
		l.save
	end

	def register
		if user_exists?
			@code = 1
		elsif !app_exists?
			@code = 2
		else	0
			u = User.new(email: params[:email], password: params[:password], username: params[:username])
			@code = u.valid? ? 0 : 2
			@token = @code == 0 ? get_token : nil
			u.save
			create_log(params[:app_id], params[:email], @token)
		end	
		@data = params		
		render_response(@code, @token)
		
	end
	
end
