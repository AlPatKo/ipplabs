class App < ActiveRecord::Base
	validates :app_id, presence: true, uniqueness: true
	validates :app_name, presence: true, uniqueness: true
end
