class User < ActiveRecord::Base
	validates :email, presence: true, uniqueness: true, format: { with: /\A[a-z0-9.-]{3,}@[a-z0-9.]{3,}[.][a-z]{2,4}\z/}
	validates :password, presence:true, format: {with: /\A[\W\w]{8,}\z/}
	validates :username, presence: true, uniqueness: true
end
