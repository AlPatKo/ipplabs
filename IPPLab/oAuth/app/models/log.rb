class Log < ActiveRecord::Base
	validates :app_id, presence: true
	validates :email, presence: true
	validates :token, presence: true, uniqueness: true
	validates :time, presence: true
end
