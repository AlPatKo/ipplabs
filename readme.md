# Lab#0 Prototyping

## Task 1

For the purpose of implementing oAuth service, I chose Ruby on Rails.

In order to start the app itself, you have to clone the repository to your local machine and then run `$ rails s`.
After doing this you can access any of three functions

1. `localhost:3000/register` for registering a user | JSON fields: {app_id, email, password, username}
2. `localhost:3000/login`	to authenticate a user | JSON fields: {app_id, email, password}
3. `localhost:3000/last`		to get last login time | JSON fields: {app_id, email, token}

Return codes

1. 0 OK (universal)
2. 1 user exists (`register`)
3. 2 invalid data (universal)
4. 3 invalid token (`last`)

cURL template I used for testing

`curl -H 'Content-type: application/json' -d '{"app_id":0,"email":"misha@gmail.com","token":"f14894c63eae3c8244620b5ddf8cb496"}' -X GET localhost:3000/last`

To create more "App" entries:

`$ rails c`

`$ App.create app_id: id, app_name: name`

## Task 2

Question that have arised during this lab:

1. How do we ensure validity of email, and whether login should be via email?

# Lab#1 Testing & Diagrams

Added tests for key component actions of controller, such as token validity check, password check, user and application existence check, log creation.

## Sequence Diagrams

![Registration](http://i.imgur.com/GH7aNgI.png?1)
![Login](http://i.imgur.com/jtbqe7f.png?1)
![Last Login](http://i.imgur.com/ZO7BPby.png?1)